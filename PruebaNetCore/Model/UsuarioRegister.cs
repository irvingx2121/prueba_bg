﻿using System.ComponentModel.DataAnnotations;

namespace PruebaNetCore.Model
{
    public class UsuarioRegister
    {
        [Required(ErrorMessage = "El campo Nombre es obligatorio")]
        public string? Nombre { get; set; }

        [Required(ErrorMessage = "El campo Email es obligatorio")]
        public string? Email { get; set; }

        [Required(ErrorMessage = "El campo Plan es obligatorio")]
        public Double? Plam { get; set; }

        [Required(ErrorMessage = "El campo Telefono es obligatorio")]
        public string? Telefono { get; set; }

        [Required(ErrorMessage = "El campo Contraseña es obligatorio")]
        public string? Contrasenia { get; set; }
    }
}
