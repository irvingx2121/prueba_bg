﻿namespace PruebaNetCore.Model
{
    public class ProductoModel
    {
        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public Double? Precio { get; set; }
        public Boolean? Estado  { get; set; }
        public string? Detalle { get; set; }
    }
}
