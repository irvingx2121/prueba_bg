﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PruebaNetCore.Datos;
using PruebaNetCore.Model;
using PruebaNetCore.Utils;

namespace PruebaNetCore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    //[EnableCors("ReglasCors")]
    //[Route("api/[controller]")]
    //[Authorize]
    //[ApiController]
    public class CotactoController : ControllerBase
    {
        ContactoDatos c = new ContactoDatos();

        [HttpGet(Name = "contacto")]
        public ActionResult Get()
        {
            List<ContactoModel> list = c.Listar();

            if (list.Count == 0) {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "La lista de contactos se encuentra vacia"));
            }

            return Ok(new Response(200, "La consulta se realizó conrrectamente", list));
        }

        [HttpPost(Name = "contacto")]
        public ActionResult Post([FromBody]ContactoModel contacto)
        {
            Boolean status =  c.Guardar(contacto);

            if (!status)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "Hubo un error al crear el contacto"));
            }

            return Ok(new Response(200, "Se creó conrrectamente el contacto", contacto));
        }

        [HttpPut(Name = "contacto")]
        public ActionResult Put([FromBody] ContactoModel contacto)
        {
            Boolean status = c.Editar(contacto);

            if (!status)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "Hubo un error al actualizar el contacto"));
            }

            return Ok(new Response(200, "Se actualizó conrrectamente el contacto", contacto));
        }

        [HttpDelete(Name = "contacto/{id}")]
        public ActionResult Delete(int id)
        {
            ContactoModel contacto = c.Obtener(id);
            
            if (contacto.IdContacto == 0) {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "No se encontró el contacto a eliminar"));
            }

            Boolean status = c.Eliminar(id);

            if (!status)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "Hubo un error al eliminar el contacto"));
            }

            return Ok(new Response(200, "Se elimino conrrectamente el contacto"));
        }
    }
}
