﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PruebaNetCore.Datos;
using PruebaNetCore.Model;
using PruebaNetCore.Utils;

namespace PruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        ProductoDatos c = new ProductoDatos();

        [HttpGet(Name = "producto")]
        public ActionResult Get()
        {
            List<ProductoModel> list = c.Listar();

            if (list.Count == 0)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "La lista de contactos se encuentra vacia"));
            }

            return Ok(new Response(200, "La consulta se realizó conrrectamente", list));
        }
    }
}
