﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PruebaNetCore.Datos;
using PruebaNetCore.Model;
using PruebaNetCore.Utils;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace PruebaNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        UsuarioDatos _user = new UsuarioDatos();

        private readonly string secretKey;

        public AuthenticateController(IConfiguration config)
        {
            secretKey = config.GetSection("settings").GetSection("secretKey").ToString();
        }

        [HttpPost]
        [Route("Validar")]
        public IActionResult Validar([FromBody] Usuario request)
        {
            UsuarioRegister user = _user.Obtener(request.email, request.password);

            if (user.Email != null)
            {
                var keyBytes = Encoding.ASCII.GetBytes(secretKey);
                var claims = new ClaimsIdentity();
                claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, request.email));
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = claims,
                    Expires = DateTime.UtcNow.AddMinutes(5),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyBytes), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenConfig = tokenHandler.CreateToken(tokenDescriptor);
                string tokencreado = tokenHandler.WriteToken(tokenConfig);

                return StatusCode(StatusCodes.Status200OK, new { codigoRetorno= StatusCodes.Status200OK,
                    mensajeRetorno = "consulta correcta",
                    usuario = user, token = tokencreado });
            }
            else
            {
                return StatusCode(StatusCodes.Status401Unauthorized,new Response(StatusCodes.Status401Unauthorized, "No se encontro el usuario"));
            }
        }

        [HttpPost]
        [Route("register")]
        public ActionResult Register([FromBody] UsuarioRegister usuario)
        {
            Boolean status = _user.Guardar(usuario);

            if (!status)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new Response(400, "Hubo un error al crear el usuario"));
            }

            return Ok(new Response(200, "Se registró correctamente el usuario", usuario));
        }
    }
}
