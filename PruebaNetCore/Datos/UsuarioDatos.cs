﻿using PruebaNetCore.Model;
using System.Data.SqlClient;
using System.Data;

namespace PruebaNetCore.Datos
{
    public class UsuarioDatos
    {
        public UsuarioRegister Obtener(string Email, string Contrasenia)
        {
            var user = new UsuarioRegister();
            var cn = new Conexion();
            using (var conexion = new SqlConnection(cn.getCadenaSQL()))
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand("sp_login", conexion);
                cmd.Parameters.AddWithValue("Email", Email);
                cmd.Parameters.AddWithValue("Contrasenia", Contrasenia);
                cmd.CommandType = CommandType.StoredProcedure;
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        user.Nombre = dr["Nombre"].ToString();
                        user.Plam = Double.Parse(dr["Plam"].ToString());
                        user.Telefono = dr["Telefono"].ToString();
                        user.Email = dr["Email"].ToString();
                    }
                }
            }
            return user;
        }
        
        public bool Guardar(UsuarioRegister usuario)
        {
            bool rpta;
            try
            {
                var cn = new Conexion();
                using (var conexion = new SqlConnection(cn.getCadenaSQL()))
                {
                    conexion.Open();
                    SqlCommand cmd = new SqlCommand("sp_regiter", conexion);
                    cmd.Parameters.AddWithValue("Nombre", usuario.Nombre);
                    cmd.Parameters.AddWithValue("Telefono",usuario.Telefono);
                    cmd.Parameters.AddWithValue("Email", usuario.Email);
                    cmd.Parameters.AddWithValue("Contrasenia", usuario.Contrasenia);
                    cmd.Parameters.AddWithValue("Plam", usuario.Plam);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
                rpta = true;
            }
            catch (Exception e)
            {
                string error = e.Message;
                rpta = false;
            }
            return rpta;
        }
    }
}
