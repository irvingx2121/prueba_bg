﻿using PruebaNetCore.Model;
using System.Data.SqlClient;
using System.Data;

namespace PruebaNetCore.Datos
{
    public class ProductoDatos
    {
        public List<ProductoModel> Listar()
        {
            var oLista = new List<ProductoModel>();
            var cn = new Conexion();
            using (var conexion = new SqlConnection(cn.getCadenaSQL()))
            {
                conexion.Open();
                SqlCommand cmd = new SqlCommand("sp_Listar_Producto", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        oLista.Add(new ProductoModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Descripcion = dr["Descripcion"].ToString(),
                            Precio = Double.Parse(dr["Precio"].ToString()),
                            Detalle = dr["Detalle"].ToString(),
                            Estado = (dr["Estado"].ToString() == "1") ? true : false
                        });
                    }
                }
            }
            return oLista;
        }
    }
}
