﻿namespace PruebaNetCore.Utils
{
    public class Response
    {

        public int codigoRetorno { get; set; }
        public string mensajeRetorno { get; set; }
        public Object respuesta { get; set; }

        public Response(int codigoRetorno, string mensajeRetorno, object respuesta)
        {
            this.codigoRetorno = codigoRetorno;
            this.mensajeRetorno = mensajeRetorno;
            this.respuesta = respuesta;
        }

        public Response(int codigoRetorno, string mensajeRetorno)
        {
            this.codigoRetorno = codigoRetorno;
            this.mensajeRetorno = mensajeRetorno;
        }
    }
}
